public class RockPaperScissors {
  public static String rps(String p1, String p2) {
    String res = "";
    if ((p1 == "scissors" && p2 == "paper") || (p1 == "rock" && p2 == "scissors") || (p1 == "paper" && p2 == "rock")) {
      res = "Player 1 won!";
    } else if (p1 == p2) {
      res = "Draw!";
    } else {
      res = "Player 2 won!";
    }
    return res;
  }
}