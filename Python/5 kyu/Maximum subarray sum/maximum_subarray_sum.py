import unittest


def max_sequence(arr):
    best_sum = 0
    current_sum = 0
    for x in arr:
        current_sum = max(0, current_sum + x)
        best_sum = max(best_sum, current_sum)
    return best_sum


class MaxSequenceTests(unittest.TestCase):

    def test_max_sequence(self):

        # Arrange
        arr = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

        # Act
        result = max_sequence(arr)

        # Assert
        self.assertEqual(result, 6, 'Should be 6')

    def test_max_sequence_negative(self):

        # Arrange
        arr = [-2, -3, -1, -5]

        # Act
        result = max_sequence(arr)

        # Assert
        self.assertEqual(result, 0, 'Should be 0')

    def test_max_sequence_empty(self):

        # Arrange
        arr = []

        # Act
        result = max_sequence(arr)

        # Assert
        self.assertEqual(result, 0, 'Should be 0')


if __name__ == "__main__":
    unittest.main()