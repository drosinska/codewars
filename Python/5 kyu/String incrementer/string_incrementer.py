import unittest


def increment_string(strng):
    head = strng.rstrip('0123456789')
    tail = strng[len(head):]
    if not tail:
        return strng + "1"
    else:
        return head + str(int(tail) + 1).zfill(len(tail))


class StringIncrementerTests(unittest.TestCase):

    def test_increment_string_without_number(self):

        # Arrange
        strng = 'foo'

        # Act
        result = increment_string(strng)

        # Assert
        self.assertEqual(result, 'foo1', 'Should be foo1')

    def test_increment_string_with_number(self):

        # Arrange
        strng = 'foobar23'

        # Act
        result = increment_string(strng)

        # Assert
        self.assertEqual(result, 'foobar24', 'Should be foobar24')


if __name__ == "__main__":
    unittest.main()
