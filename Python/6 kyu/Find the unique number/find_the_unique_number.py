import unittest


def find_uniq(arr):
    res = sorted(arr)
    return res[0] if res[0] != res[1] else res[-1]


class FindUniqTest(unittest.TestCase):

    def test_find_unique_number(self):

        # Arrange
        arr = [1, 2, 1]

        # Act
        result = find_uniq(arr)

        # Assert
        self.assertEqual(result, 2, 'Should be 2')


if __name__ == "__main__":
    unittest.main()
