import unittest


def solution(number):
    return sum(i for i in range(number) if not i % 5 or not i % 3)


class SolutionTests(unittest.TestCase):

    def test_positive_number(self):

        # Arrange
        number = 10

        # Act
        result = solution(number)

        # Assert
        self.assertEqual(result, 23, 'Should be 23')

    def test_negative_number(self):

        # Arrange
        number = -10

        # Act
        result = solution(number)

        # Assert
        self.assertEqual(result, 0, 'Should be 0')


if __name__ == "__main__":
    unittest.main()
