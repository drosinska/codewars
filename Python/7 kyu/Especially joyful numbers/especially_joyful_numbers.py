import unittest


def number_joy(n):
    digit_sum = sum(int(i) for i in str(n))
    digit_sum_reversed = int(str(digit_sum)[::-1])
    return digit_sum * digit_sum_reversed == n


class NumberJoyTests(unittest.TestCase):

    def test_harshad_number(self):

        # Arrange
        n = 1729

        # Act
        result = number_joy(n)

        # Assert
        self.assertEqual(result, True, 'Should be True')

    def test_not_a_harshad_number(self):

        # Arrange
        n = 1997

        # Act
        result = number_joy(n)

        # Assert
        self.assertEqual(result, False, 'Should be False')

    def test_harshad_number_incorrect_sum(self):

        # Arrange
        n = 18

        # Act
        result = number_joy(n)

        # Assert
        self.assertEqual(result, False, 'Should be False')


if __name__ == "__main__":
    unittest.main()
