import unittest
import re


def hex_hash(code):
    return sum([int(i) for i in ','.join([re.sub("\D", '', hex(ord(i))) for i in code]).replace(',', '')])


class HexHashTests(unittest.TestCase):

    def test_hex_hash_non_empty(self):

        # Arrange
        code = 'kcxnjsklsHskjHDkl7878hHJk'

        # Act
        result = hex_hash(code)

        # Assert
        self.assertEqual(result, 218, 'Should be 218')

    def test_hex_hash_empty(self):
        
        # Arrange
        code = ''

        # Act
        result = hex_hash(code)

        # Assert
        self.assertEqual(result, 0, 'Should be 0')


if __name__ == "__main__":
    unittest.main()