import unittest


def is_isogram(string):
    return len(string) == len(set(string.lower()))


class IsIsogramTests(unittest.TestCase):

    def test_is_isogram(self):

        # Arrange
        string = 'fo'

        # Act
        result = is_isogram(string)

        # Assert
        self.assertEqual(result, True, 'Should be True')

    def test_is_not_isogram(self):

        # Arrange
        string = 'foO'

        # Act
        result = is_isogram(string)

        # Assert
        self.assertEqual(result, False, 'Should be False')


if __name__ == "__main__":
    unittest.main()
