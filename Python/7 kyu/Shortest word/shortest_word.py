import unittest


def find_shortest(words):
    split_words = words.split()
    shortest = len(split_words[0])
    for word in split_words:
        if len(word) < shortest:
            shortest = len(word)
    return shortest


class FindShortestTest(unittest.TestCase):

    def test_find_shortest(self):
        # Arrange
        words = "I want to travel the world writing code one day"

        # Act
        result = find_shortest(words)

        # Assert
        self.assertEqual(result, 1, 'Should be 1')


if __name__ == "__main__":
    unittest.main()
