SELECT CAST(created_at AS date) AS date, COUNT(title) AS count, 
CAST(SUM (COUNT(title)) OVER (ORDER BY CAST(created_at as date)) AS INTEGER) AS total
FROM posts
GROUP BY CAST(created_at AS date)
ORDER BY CAST(created_at AS date)