SELECT DATE_TRUNC('day', created_at) :: date AS day, description, COUNT(*) FROM events
WHERE name = 'trained'
GROUP BY day, description
ORDER BY day