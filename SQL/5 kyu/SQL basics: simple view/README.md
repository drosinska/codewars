For this challenge you need to create a view. This view is used by a sales store to give out vouches to members who have spent over $1000 in departments that have brought in more than $10000 total ordered by the members id. The view must be called `members_approved_for_voucher` then you must create a SELECT query using the view.

# resultant table schema
- id
- name
- email
- total_spending