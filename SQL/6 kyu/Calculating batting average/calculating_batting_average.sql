SELECT 
    player_name, 
    games, 
    ROUND((hits :: decimal) / (at_bats :: decimal), 3) :: varchar AS batting_average
FROM 
    yankees
WHERE
    at_bats >= 100
ORDER BY 
    batting_average DESC
