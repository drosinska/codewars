Given a payment table, which is a part of [DVD Rental Sample Database](https://www.postgresqltutorial.com/postgresql-getting-started/postgresql-sample-database/), with the following schema produce a result set for the report that shows a side-by-side comparison of the number and total amounts of payments made in Mike's and Jon's stores broken down by months.

__Payment table__

| Column       | Type                        | Modifiers   |
| ------------ | --------------------------- | ----------- |
| payment_id   | integer                     | not null    |
| customer_id  | smallint                    | not null    |
| staff_id     | smallint                    | not null    |
| rental_id    | integer                     | not null    |
| amount       | numeric(5,2)                | not null    |
| payment_date | timestamp without time zone | not null    |

The resulting data set should be ordered by month using natural order (Jan, Feb, Mar, etc.).

__Note__. You don't need to worry about the year component. Months are never repeated because the sample data set contains payment information only for one year.

The desired output for the report:

| month | total_count | total_amount | mike_count | mike_amount | jon_count | jon_amount |
| ----- | ----------- | ------------ | ---------- | ----------- | --------- | ---------- |
| 2     |             |              |            |             |           |            |
| 5     |             |              |            |             |           |            |         
...

- __month__. Number of the month (1 - January, 2 - February, etc.)
- __total_count__. Total number of payments.
- __total_amount__. Total payment amount.
- __mike_count__. Total number of payments accepted by Mike (staff_id = 1).
- __mike_amount__. Total amount of payments accepted by Mike (staff_id = 1).
- __jon_count__. Total number of payments accepted by Jon (staff_id = 2).
- __jon_amount__. Total amount of payments accepted by Jon (staff_id = 2).

