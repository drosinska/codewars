SELECT
    data ->> 'first_name' AS first_name,
    data ->> 'last_name' AS last_name,
    (EXTRACT(YEAR FROM(AGE((data ->> 'date_of_birth') :: date)))) :: int AS age,
CASE
    WHEN data ->> 'private' = 'true' THEN 'Hidden'
    WHEN json_array_length(data -> 'email_addresses') = '0' THEN 'None'
    ELSE TRIM(BOTH '"' FROM((data -> 'email_addresses'-> 0) :: text))
END AS email_address
FROM
    users
ORDER BY
    first_name, last_name
