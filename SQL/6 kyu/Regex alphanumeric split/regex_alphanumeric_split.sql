SELECT
    project, 
    REGEXP_REPLACE(address, '\d', '', 'g') AS letters, 
    REGEXP_REPLACE(address, '\D', '', 'g') AS numbers 
FROM
    repositories
