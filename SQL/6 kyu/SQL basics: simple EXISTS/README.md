For this challenge you need to create a SELECT statement that will contain data about departments that had a sale with a price over 98.00 dollars. This SELECT statement will have to use an EXISTS to achieve the task.

__departments table schema__
- id
- name

__sales table schema__
- id
- department_id (department foreign key)
- name
- price
- card_name
- card_number
- transaction_date

__resultant table schema__
- id
- name
