SELECT
    people.id,
    people.name,
    COUNT(sales.id) sale_count,
    RANK() OVER (PARTITION BY COUNT(sales.id) ORDER BY COUNT(sales.id)) sale_rank
    FROM people
JOIN
    sales
ON
    people.id = sales.people_id
GROUP BY
    people.id,
    people.name
