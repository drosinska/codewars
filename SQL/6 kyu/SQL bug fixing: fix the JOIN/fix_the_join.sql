SELECT
    job.job_title,
    ROUND(AVG(job.salary), 2)::FLOAT AS average_salary,
    COUNT(people_id) AS total_people,
    ROUND(SUM(salary), 2)::FLOAT AS total_salary
FROM
    job
JOIN
    people
ON
    job.people_id = people.id
GROUP BY
    job.job_title
ORDER BY
    average_salary DESC
