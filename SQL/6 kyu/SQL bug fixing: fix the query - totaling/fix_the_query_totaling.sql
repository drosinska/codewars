SELECT sale.transaction_date :: date AS day, department.name AS department, COUNT(sale.id) AS sale_count 
FROM sale
JOIN department ON sale.department_id = department.id
GROUP BY day, department
ORDER BY day, department
